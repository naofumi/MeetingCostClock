package de.hoffmannsgimmicks;

/* OLEDView.java (c) 2011-2015 by Markus Hoffmann 
 *
 * This file is part of MeetingCostClock for Android 
 * ============================================================
 * MeetingCostClock for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class OLEDView extends View {
	private Paint paint;
	private int sw,sh;
	private String text="OLED";
	private int chw=5*4+2,chh=7*4+2;
	private int tc=1;
	
	/* The font is a classical 5x7 matrix font with 
	   ASCII IBM charackter set. You can replace each individual charackter
	   by replacing its 5 bytes bitmap representation. The font id based on 
	   very old charackters found in public domain. */
	
	private final static byte[] font={
		/* Charackter #0  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #1  */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x5b, /*   ##  ####  #### */
		(byte) 0x4f, /*   ##    ######## */
		(byte) 0x5b, /*   ##  ####  #### */
		(byte) 0x3e, /*     ##########   */
		/* Charackter #2  */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x6b, /*   ####  ##  #### */
		(byte) 0x4f, /*   ##    ######## */
		(byte) 0x6b, /*   ####  ##  #### */
		(byte) 0x3e, /*     ##########   */
		/* Charackter #3  */
		(byte) 0x1c, /*       ######     */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x1c, /*       ######     */
		/* Charackter #4  */
		(byte) 0x18, /*       ####       */
		(byte) 0x3c, /*     ########     */
		(byte) 0x7e, /*   ############   */
		(byte) 0x3c, /*     ########     */
		(byte) 0x18, /*       ####       */
		/* Charackter #5  */
		(byte) 0x1c, /*       ######     */
		(byte) 0x57, /*   ##  ##  ###### */
		(byte) 0x7d, /*   ##########  ## */
		(byte) 0x57, /*   ##  ##  ###### */
		(byte) 0x1c, /*       ######     */
		/* Charackter #6  */
		(byte) 0x1c, /*       ######     */
		(byte) 0x5e, /*   ##  ########   */
		(byte) 0x7f, /*   ############## */
		(byte) 0x5e, /*   ##  ########   */
		(byte) 0x1c, /*       ######     */
		/* Charackter #7  */
		(byte) 0x00, /*                  */
		(byte) 0x18, /*       ####       */
		(byte) 0x3c, /*     ########     */
		(byte) 0x18, /*       ####       */
		(byte) 0x00, /*                  */
		/* Charackter #8  */
		(byte) 0xff, /* ################ */
		(byte) 0xe7, /* ######    ###### */
		(byte) 0xc3, /* ####        #### */
		(byte) 0xe7, /* ######    ###### */
		(byte) 0xff, /* ################ */
		/* Charackter #9  */
		(byte) 0x00, /*                  */
		(byte) 0x18, /*       ####       */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0x18, /*       ####       */
		(byte) 0x00, /*                  */
		/* Charackter #10  */
		(byte) 0xff, /* ################ */
		(byte) 0xe7, /* ######    ###### */
		(byte) 0xdb, /* ####  ####  #### */
		(byte) 0xe7, /* ######    ###### */
		(byte) 0xff, /* ################ */
		/* Charackter #11  */
		(byte) 0x30, /*     ####         */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x3a, /*     ######  ##   */
		(byte) 0x06, /*           ####   */
		(byte) 0x0e, /*         ######   */
		/* Charackter #12  */
		(byte) 0x26, /*     ##    ####   */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x79, /*   ########    ## */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x26, /*     ##    ####   */
		/* Charackter #13  */
		(byte) 0x40, /*   ##             */
		(byte) 0x7f, /*   ############## */
		(byte) 0x05, /*           ##  ## */
		(byte) 0x05, /*           ##  ## */
		(byte) 0x07, /*           ###### */
		/* Charackter #14  */
		(byte) 0x40, /*   ##             */
		(byte) 0x7f, /*   ############## */
		(byte) 0x05, /*           ##  ## */
		(byte) 0x25, /*     ##    ##  ## */
		(byte) 0x3f, /*     ############ */
		/* Charackter #15  */
		(byte) 0x5a, /*   ##  ####  ##   */
		(byte) 0x3c, /*     ########     */
		(byte) 0xe7, /* ######    ###### */
		(byte) 0x3c, /*     ########     */
		(byte) 0x5a, /*   ##  ####  ##   */
		/* Charackter #16  */
		(byte) 0x7f, /*   ############## */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x1c, /*       ######     */
		(byte) 0x1c, /*       ######     */
		(byte) 0x08, /*         ##       */
		/* Charackter #17  */
		(byte) 0x08, /*         ##       */
		(byte) 0x1c, /*       ######     */
		(byte) 0x1c, /*       ######     */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x7f, /*   ############## */
		/* Charackter #18  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x7f, /*   ############## */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #19  */
		(byte) 0x5f, /*   ##  ########## */
		(byte) 0x5f, /*   ##  ########## */
		(byte) 0x00, /*                  */
		(byte) 0x5f, /*   ##  ########## */
		(byte) 0x5f, /*   ##  ########## */
		/* Charackter #20  */
		(byte) 0x06, /*           ####   */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x7f, /*   ############## */
		(byte) 0x01, /*               ## */
		(byte) 0x7f, /*   ############## */
		/* Charackter #21  */
		(byte) 0x00, /*                  */
		(byte) 0x66, /*   ####    ####   */
		(byte) 0x89, /* ##      ##    ## */
		(byte) 0x95, /* ##    ##  ##  ## */
		(byte) 0x6a, /*   ####  ##  ##   */
		/* Charackter #22  */
		(byte) 0x60, /*   ####           */
		(byte) 0x60, /*   ####           */
		(byte) 0x60, /*   ####           */
		(byte) 0x60, /*   ####           */
		(byte) 0x60, /*   ####           */
		/* Charackter #23  */
		(byte) 0x94, /* ##    ##  ##     */
		(byte) 0xa2, /* ##  ##      ##   */
		(byte) 0xff, /* ################ */
		(byte) 0xa2, /* ##  ##      ##   */
		(byte) 0x94, /* ##    ##  ##     */
		/* Charackter #24  */
		(byte) 0x08, /*         ##       */
		(byte) 0x04, /*           ##     */
		(byte) 0x7e, /*   ############   */
		(byte) 0x04, /*           ##     */
		(byte) 0x08, /*         ##       */
		/* Charackter #25  */
		(byte) 0x10, /*       ##         */
		(byte) 0x20, /*     ##           */
		(byte) 0x7e, /*   ############   */
		(byte) 0x20, /*     ##           */
		(byte) 0x10, /*       ##         */
		/* Charackter #26  */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x2a, /*     ##  ##  ##   */
		(byte) 0x1c, /*       ######     */
		(byte) 0x08, /*         ##       */
		/* Charackter #27  */
		(byte) 0x08, /*         ##       */
		(byte) 0x1c, /*       ######     */
		(byte) 0x2a, /*     ##  ##  ##   */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		/* Charackter #28  */
		(byte) 0x1e, /*       ########   */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		/* Charackter #29  */
		(byte) 0x0c, /*         ####     */
		(byte) 0x1e, /*       ########   */
		(byte) 0x0c, /*         ####     */
		(byte) 0x1e, /*       ########   */
		(byte) 0x0c, /*         ####     */
		/* Charackter #30  */
		(byte) 0x30, /*     ####         */
		(byte) 0x38, /*     ######       */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x38, /*     ######       */
		(byte) 0x30, /*     ####         */
		/* Charackter #31  */
		(byte) 0x06, /*           ####   */
		(byte) 0x0e, /*         ######   */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x0e, /*         ######   */
		(byte) 0x06, /*           ####   */
		/* Charackter #32   */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #33 ! */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x5f, /*   ##  ########## */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #34 " */
		(byte) 0x00, /*                  */
		(byte) 0x07, /*           ###### */
		(byte) 0x00, /*                  */
		(byte) 0x07, /*           ###### */
		(byte) 0x00, /*                  */
		/* Charackter #35 # */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x7f, /*   ############## */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x7f, /*   ############## */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #36 $ */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0x2a, /*     ##  ##  ##   */
		(byte) 0x7f, /*   ############## */
		(byte) 0x2a, /*     ##  ##  ##   */
		(byte) 0x12, /*       ##    ##   */
		/* Charackter #37 % */
		(byte) 0x23, /*     ##      #### */
		(byte) 0x13, /*       ##    #### */
		(byte) 0x08, /*         ##       */
		(byte) 0x64, /*   ####    ##     */
		(byte) 0x62, /*   ####      ##   */
		/* Charackter #38 & */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x50, /*   ##  ##         */
		/* Charackter #39 ' */
		(byte) 0x00, /*                  */
		(byte) 0x05, /*           ##  ## */
		(byte) 0x03, /*             #### */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #40 ( */
		(byte) 0x00, /*                  */
		(byte) 0x1c, /*       ######     */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x00, /*                  */
		/* Charackter #41 ) */
		(byte) 0x00, /*                  */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x1c, /*       ######     */
		(byte) 0x00, /*                  */
		/* Charackter #42 * */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x08, /*         ##       */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x08, /*         ##       */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #43 + */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		/* Charackter #44 , */
		(byte) 0x00, /*                  */
		(byte) 0x50, /*   ##  ##         */
		(byte) 0x30, /*     ####         */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #45 - */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		/* Charackter #46 . */
		(byte) 0x00, /*                  */
		(byte) 0x60, /*   ####           */
		(byte) 0x60, /*   ####           */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #47 / */
		(byte) 0x20, /*     ##           */
		(byte) 0x10, /*       ##         */
		(byte) 0x08, /*         ##       */
		(byte) 0x04, /*           ##     */
		(byte) 0x02, /*             ##   */
		/* Charackter #48 0 */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x51, /*   ##  ##      ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x3e, /*     ##########   */
		/* Charackter #49 1 */
		(byte) 0x00, /*                  */
		(byte) 0x42, /*   ##        ##   */
		(byte) 0x7f, /*   ############## */
		(byte) 0x40, /*   ##             */
		(byte) 0x00, /*                  */
		/* Charackter #50 2 */
		(byte) 0x42, /*   ##        ##   */
		(byte) 0x61, /*   ####        ## */
		(byte) 0x51, /*   ##  ##      ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x46, /*   ##      ####   */
		/* Charackter #51 3 */
		(byte) 0x21, /*     ##        ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x4b, /*   ##    ##  #### */
		(byte) 0x31, /*     ####      ## */
		/* Charackter #52 4 */
		(byte) 0x18, /*       ####       */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x12, /*       ##    ##   */
		(byte) 0x7f, /*   ############## */
		(byte) 0x10, /*       ##         */
		/* Charackter #53 5 */
		(byte) 0x27, /*     ##    ###### */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x39, /*     ######    ## */
		/* Charackter #54 6 */
		(byte) 0x3c, /*     ########     */
		(byte) 0x4a, /*   ##    ##  ##   */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x30, /*     ####         */
		/* Charackter #55 7 */
		(byte) 0x01, /*               ## */
		(byte) 0x71, /*   ######      ## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x05, /*           ##  ## */
		(byte) 0x03, /*             #### */
		/* Charackter #56 8 */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x36, /*     ####  ####   */
		/* Charackter #57 9 */
		(byte) 0x06, /*           ####   */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x1e, /*       ########   */
		/* Charackter #58 : */
		(byte) 0x00, /*                  */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #59 ; */
		(byte) 0x00, /*                  */
		(byte) 0x56, /*   ##  ##  ####   */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #60 < */
		(byte) 0x08, /*         ##       */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x00, /*                  */
		/* Charackter #61 = */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #62 > */
		(byte) 0x00, /*                  */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x08, /*         ##       */
		/* Charackter #63 ? */
		(byte) 0x02, /*             ##   */
		(byte) 0x01, /*               ## */
		(byte) 0x51, /*   ##  ##      ## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x06, /*           ####   */
		/* Charackter #64 @ */
		(byte) 0x32, /*     ####    ##   */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x79, /*   ########    ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x3e, /*     ##########   */
		/* Charackter #65 A */
		(byte) 0x7e, /*   ############   */
		(byte) 0x11, /*       ##      ## */
		(byte) 0x11, /*       ##      ## */
		(byte) 0x11, /*       ##      ## */
		(byte) 0x7e, /*   ############   */
		/* Charackter #66 B */
		(byte) 0x7f, /*   ############## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x36, /*     ####  ####   */
		/* Charackter #67 C */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x22, /*     ##      ##   */
		/* Charackter #68 D */
		(byte) 0x7f, /*   ############## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x1c, /*       ######     */
		/* Charackter #69 E */
		(byte) 0x7f, /*   ############## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x41, /*   ##          ## */
		/* Charackter #70 F */
		(byte) 0x7f, /*   ############## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x01, /*               ## */
		/* Charackter #71 G */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x7a, /*   ########  ##   */
		/* Charackter #72 H */
		(byte) 0x7f, /*   ############## */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x7f, /*   ############## */
		/* Charackter #73 I */
		(byte) 0x00, /*                  */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x7f, /*   ############## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x00, /*                  */
		/* Charackter #74 J */
		(byte) 0x20, /*     ##           */
		(byte) 0x40, /*   ##             */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x3f, /*     ############ */
		(byte) 0x01, /*               ## */
		/* Charackter #75 K */
		(byte) 0x7f, /*   ############## */
		(byte) 0x08, /*         ##       */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x41, /*   ##          ## */
		/* Charackter #76 L */
		(byte) 0x7f, /*   ############## */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		/* Charackter #77 M */
		(byte) 0x7f, /*   ############## */
		(byte) 0x02, /*             ##   */
		(byte) 0x0c, /*         ####     */
		(byte) 0x02, /*             ##   */
		(byte) 0x7f, /*   ############## */
		/* Charackter #78 N */
		(byte) 0x7f, /*   ############## */
		(byte) 0x04, /*           ##     */
		(byte) 0x08, /*         ##       */
		(byte) 0x10, /*       ##         */
		(byte) 0x7f, /*   ############## */
		/* Charackter #79 O */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x3e, /*     ##########   */
		/* Charackter #80 P */
		(byte) 0x7f, /*   ############## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x06, /*           ####   */
		/* Charackter #81 Q */
		(byte) 0x3e, /*     ##########   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x51, /*   ##  ##      ## */
		(byte) 0x21, /*     ##        ## */
		(byte) 0x5e, /*   ##  ########   */
		/* Charackter #82 R */
		(byte) 0x7f, /*   ############## */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x19, /*       ####    ## */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x46, /*   ##      ####   */
		/* Charackter #83 S */
		(byte) 0x46, /*   ##      ####   */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x31, /*     ####      ## */
		/* Charackter #84 T */
		(byte) 0x01, /*               ## */
		(byte) 0x01, /*               ## */
		(byte) 0x7f, /*   ############## */
		(byte) 0x01, /*               ## */
		(byte) 0x01, /*               ## */
		/* Charackter #85 U */
		(byte) 0x3f, /*     ############ */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x3f, /*     ############ */
		/* Charackter #86 V */
		(byte) 0x1f, /*       ########## */
		(byte) 0x20, /*     ##           */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		(byte) 0x1f, /*       ########## */
		/* Charackter #87 W */
		(byte) 0x3f, /*     ############ */
		(byte) 0x40, /*   ##             */
		(byte) 0x38, /*     ######       */
		(byte) 0x40, /*   ##             */
		(byte) 0x3f, /*     ############ */
		/* Charackter #88 X */
		(byte) 0x63, /*   ####      #### */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x08, /*         ##       */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x63, /*   ####      #### */
		/* Charackter #89 Y */
		(byte) 0x07, /*           ###### */
		(byte) 0x08, /*         ##       */
		(byte) 0x70, /*   ######         */
		(byte) 0x08, /*         ##       */
		(byte) 0x07, /*           ###### */
		/* Charackter #90 Z */
		(byte) 0x61, /*   ####        ## */
		(byte) 0x51, /*   ##  ##      ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x43, /*   ##        #### */
		/* Charackter #91 [ */
		(byte) 0x00, /*                  */
		(byte) 0x7f, /*   ############## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x00, /*                  */
		/* Charackter #92 \ */
		(byte) 0x02, /*             ##   */
		(byte) 0x04, /*           ##     */
		(byte) 0x08, /*         ##       */
		(byte) 0x10, /*       ##         */
		(byte) 0x20, /*     ##           */
		/* Charackter #93 ] */
		(byte) 0x00, /*                  */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x7f, /*   ############## */
		(byte) 0x00, /*                  */
		/* Charackter #94 ^ */
		(byte) 0x04, /*           ##     */
		(byte) 0x02, /*             ##   */
		(byte) 0x01, /*               ## */
		(byte) 0x02, /*             ##   */
		(byte) 0x04, /*           ##     */
		/* Charackter #95 _ */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		/* Charackter #96 ` */
		(byte) 0x00, /*                  */
		(byte) 0x01, /*               ## */
		(byte) 0x02, /*             ##   */
		(byte) 0x04, /*           ##     */
		(byte) 0x00, /*                  */
		/* Charackter #97 a */
		(byte) 0x20, /*     ##           */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x78, /*   ########       */
		/* Charackter #98 b */
		(byte) 0x7f, /*   ############## */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x38, /*     ######       */
		/* Charackter #99 c */
		(byte) 0x38, /*     ######       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x20, /*     ##           */
		/* Charackter #100 d */
		(byte) 0x38, /*     ######       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x7f, /*   ############## */
		/* Charackter #101 e */
		(byte) 0x38, /*     ######       */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x18, /*       ####       */
		/* Charackter #102 f */
		(byte) 0x08, /*         ##       */
		(byte) 0x7e, /*   ############   */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x01, /*               ## */
		(byte) 0x02, /*             ##   */
		/* Charackter #103 g */
		(byte) 0x0c, /*         ####     */
		(byte) 0x52, /*   ##  ##    ##   */
		(byte) 0x52, /*   ##  ##    ##   */
		(byte) 0x52, /*   ##  ##    ##   */
		(byte) 0x3e, /*     ##########   */
		/* Charackter #104 h */
		(byte) 0x7f, /*   ############## */
		(byte) 0x08, /*         ##       */
		(byte) 0x04, /*           ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0x78, /*   ########       */
		/* Charackter #105 i */
		(byte) 0x00, /*                  */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x7d, /*   ##########  ## */
		(byte) 0x40, /*   ##             */
		(byte) 0x00, /*                  */
		/* Charackter #106 j */
		(byte) 0x20, /*     ##           */
		(byte) 0x40, /*   ##             */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x3d, /*     ########  ## */
		(byte) 0x00, /*                  */
		/* Charackter #107 k */
		(byte) 0x7f, /*   ############## */
		(byte) 0x10, /*       ##         */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x00, /*                  */
		/* Charackter #108 l */
		(byte) 0x00, /*                  */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x7f, /*   ############## */
		(byte) 0x40, /*   ##             */
		(byte) 0x00, /*                  */
		/* Charackter #109 m */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x04, /*           ##     */
		(byte) 0x18, /*       ####       */
		(byte) 0x04, /*           ##     */
		(byte) 0x78, /*   ########       */
		/* Charackter #110 n */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x08, /*         ##       */
		(byte) 0x04, /*           ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0x78, /*   ########       */
		/* Charackter #111 o */
		(byte) 0x38, /*     ######       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x38, /*     ######       */
		/* Charackter #112 p */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x08, /*         ##       */
		/* Charackter #113 q */
		(byte) 0x08, /*         ##       */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x18, /*       ####       */
		(byte) 0x7c, /*   ##########     */
		/* Charackter #114 r */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x08, /*         ##       */
		(byte) 0x04, /*           ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0x08, /*         ##       */
		/* Charackter #115 s */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x20, /*     ##           */
		/* Charackter #116 t */
		(byte) 0x04, /*           ##     */
		(byte) 0x3f, /*     ############ */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		/* Charackter #117 u */
		(byte) 0x3c, /*     ########     */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		(byte) 0x7c, /*   ##########     */
		/* Charackter #118 v */
		(byte) 0x1c, /*       ######     */
		(byte) 0x20, /*     ##           */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		(byte) 0x1c, /*       ######     */
		/* Charackter #119 w */
		(byte) 0x3c, /*     ########     */
		(byte) 0x40, /*   ##             */
		(byte) 0x30, /*     ####         */
		(byte) 0x40, /*   ##             */
		(byte) 0x3c, /*     ########     */
		/* Charackter #120 x */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x10, /*       ##         */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x44, /*   ##      ##     */
		/* Charackter #121 y */
		(byte) 0x0c, /*         ####     */
		(byte) 0x50, /*   ##  ##         */
		(byte) 0x50, /*   ##  ##         */
		(byte) 0x50, /*   ##  ##         */
		(byte) 0x3c, /*     ########     */
		/* Charackter #122 z */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x64, /*   ####    ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x4c, /*   ##    ####     */
		(byte) 0x44, /*   ##      ##     */
		/* Charackter #123  */
		(byte) 0x00, /*                  */
		(byte) 0x08, /*         ##       */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x00, /*                  */
		/* Charackter #124  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x7f, /*   ############## */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #125  */
		(byte) 0x00, /*                  */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x08, /*         ##       */
		(byte) 0x00, /*                  */
		/* Charackter #126  */
		(byte) 0x10, /*       ##         */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x10, /*       ##         */
		(byte) 0x08, /*         ##       */
		/* Charackter #127  */
		(byte) 0x78, /*   ########       */
		(byte) 0x46, /*   ##      ####   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x46, /*   ##      ####   */
		(byte) 0x78, /*   ########       */
		/* Charackter #128  */
		(byte) 0x00, /*                  */
		(byte) 0x03, /*             #### */
		(byte) 0x07, /*           ###### */
		(byte) 0x08, /*         ##       */
		(byte) 0x00, /*                  */
		/* Charackter #129  */
		(byte) 0x20, /*     ##           */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x78, /*   ########       */
		(byte) 0x40, /*   ##             */
		/* Charackter #130  */
		(byte) 0x7f, /*   ############## */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x38, /*     ######       */
		/* Charackter #131  */
		(byte) 0x38, /*     ######       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x28, /*     ##  ##       */
		/* Charackter #132  */
		(byte) 0x38, /*     ######       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x7f, /*   ############## */
		/* Charackter #133  */
		(byte) 0x38, /*     ######       */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x18, /*       ####       */
		/* Charackter #134  */
		(byte) 0x00, /*                  */
		(byte) 0x08, /*         ##       */
		(byte) 0x7e, /*   ############   */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x02, /*             ##   */
		/* Charackter #135  */
		(byte) 0x18, /*       ####       */
		(byte) 0xa4, /* ##  ##    ##     */
		(byte) 0xa4, /* ##  ##    ##     */
		(byte) 0x9c, /* ##    ######     */
		(byte) 0x78, /*   ########       */
		/* Charackter #136  */
		(byte) 0x7f, /*   ############## */
		(byte) 0x08, /*         ##       */
		(byte) 0x04, /*           ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0x78, /*   ########       */
		/* Charackter #137  */
		(byte) 0x00, /*                  */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x7d, /*   ##########  ## */
		(byte) 0x40, /*   ##             */
		(byte) 0x00, /*                  */
		/* Charackter #138  */
		(byte) 0x20, /*     ##           */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x3d, /*     ########  ## */
		(byte) 0x00, /*                  */
		/* Charackter #139  */
		(byte) 0x7f, /*   ############## */
		(byte) 0x10, /*       ##         */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x00, /*                  */
		/* Charackter #140  */
		(byte) 0x00, /*                  */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x7f, /*   ############## */
		(byte) 0x40, /*   ##             */
		(byte) 0x00, /*                  */
		/* Charackter #141  */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x04, /*           ##     */
		(byte) 0x78, /*   ########       */
		(byte) 0x04, /*           ##     */
		(byte) 0x78, /*   ########       */
		/* Charackter #142  */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x08, /*         ##       */
		(byte) 0x04, /*           ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0x78, /*   ########       */
		/* Charackter #143  */
		(byte) 0x38, /*     ######       */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x38, /*     ######       */
		/* Charackter #144  */
		(byte) 0xfc, /* ############     */
		(byte) 0x18, /*       ####       */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0x18, /*       ####       */
		/* Charackter #145  */
		(byte) 0x18, /*       ####       */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0x18, /*       ####       */
		(byte) 0xfc, /* ############     */
		/* Charackter #146  */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x08, /*         ##       */
		(byte) 0x04, /*           ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0x08, /*         ##       */
		/* Charackter #147  */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x24, /*     ##    ##     */
		/* Charackter #148  */
		(byte) 0x04, /*           ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0x3f, /*     ############ */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x24, /*     ##    ##     */
		/* Charackter #149  */
		(byte) 0x3c, /*     ########     */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		(byte) 0x7c, /*   ##########     */
		/* Charackter #150  */
		(byte) 0x1c, /*       ######     */
		(byte) 0x20, /*     ##           */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		(byte) 0x1c, /*       ######     */
		/* Charackter #151  */
		(byte) 0x3c, /*     ########     */
		(byte) 0x40, /*   ##             */
		(byte) 0x30, /*     ####         */
		(byte) 0x40, /*   ##             */
		(byte) 0x3c, /*     ########     */
		/* Charackter #152  */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x10, /*       ##         */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x44, /*   ##      ##     */
		/* Charackter #153  */
		(byte) 0x4c, /*   ##    ####     */
		(byte) 0x90, /* ##    ##         */
		(byte) 0x90, /* ##    ##         */
		(byte) 0x90, /* ##    ##         */
		(byte) 0x7c, /*   ##########     */
		/* Charackter #154  */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x64, /*   ####    ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x4c, /*   ##    ####     */
		(byte) 0x44, /*   ##      ##     */
		/* Charackter #155  */
		(byte) 0x00, /*                  */
		(byte) 0x08, /*         ##       */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x00, /*                  */
		/* Charackter #156  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x77, /*   ######  ###### */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #157  */
		(byte) 0x00, /*                  */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x36, /*     ####  ####   */
		(byte) 0x08, /*         ##       */
		(byte) 0x00, /*                  */
		/* Charackter #158  */
		(byte) 0x02, /*             ##   */
		(byte) 0x01, /*               ## */
		(byte) 0x02, /*             ##   */
		(byte) 0x04, /*           ##     */
		(byte) 0x02, /*             ##   */
		/* Charackter #159  */
		(byte) 0x3c, /*     ########     */
		(byte) 0x26, /*     ##    ####   */
		(byte) 0x23, /*     ##      #### */
		(byte) 0x26, /*     ##    ####   */
		(byte) 0x3c, /*     ########     */
		/* Charackter #160  */
		(byte) 0x1e, /*       ########   */
		(byte) 0xa1, /* ##  ##        ## */
		(byte) 0xa1, /* ##  ##        ## */
		(byte) 0x61, /*   ####        ## */
		(byte) 0x12, /*       ##    ##   */
		/* Charackter #161  */
		(byte) 0x3a, /*     ######  ##   */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		(byte) 0x7a, /*   ########  ##   */
		/* Charackter #162  */
		(byte) 0x38, /*     ######       */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x59, /*   ##  ####    ## */
		/* Charackter #163  */
		(byte) 0x21, /*     ##        ## */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x79, /*   ########    ## */
		(byte) 0x41, /*   ##          ## */
		/* Charackter #164  */
		(byte) 0x21, /*     ##        ## */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x78, /*   ########       */
		(byte) 0x41, /*   ##          ## */
		/* Charackter #165  */
		(byte) 0x21, /*     ##        ## */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x78, /*   ########       */
		(byte) 0x40, /*   ##             */
		/* Charackter #166  */
		(byte) 0x20, /*     ##           */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x79, /*   ########    ## */
		(byte) 0x40, /*   ##             */
		/* Charackter #167  */
		(byte) 0x0c, /*         ####     */
		(byte) 0x1e, /*       ########   */
		(byte) 0x52, /*   ##  ##    ##   */
		(byte) 0x72, /*   ######    ##   */
		(byte) 0x12, /*       ##    ##   */
		/* Charackter #168  */
		(byte) 0x39, /*     ######    ## */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x59, /*   ##  ####    ## */
		/* Charackter #169  */
		(byte) 0x39, /*     ######    ## */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x59, /*   ##  ####    ## */
		/* Charackter #170  */
		(byte) 0x39, /*     ######    ## */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x58, /*   ##  ####       */
		/* Charackter #171  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x41, /*   ##          ## */
		/* Charackter #172  */
		(byte) 0x00, /*                  */
		(byte) 0x02, /*             ##   */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x7d, /*   ##########  ## */
		(byte) 0x42, /*   ##        ##   */
		/* Charackter #173  */
		(byte) 0x00, /*                  */
		(byte) 0x01, /*               ## */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x40, /*   ##             */
		/* Charackter #174  */
		(byte) 0xf0, /* ########         */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0xf0, /* ########         */
		/* Charackter #175  */
		(byte) 0xf0, /* ########         */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x25, /*     ##    ##  ## */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0xf0, /* ########         */
		/* Charackter #176  */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x45, /*   ##      ##  ## */
		(byte) 0x00, /*                  */
		/* Charackter #177  */
		(byte) 0x20, /*     ##           */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x54, /*   ##  ##  ##     */
		/* Charackter #178  */
		(byte) 0x7c, /*   ##########     */
		(byte) 0x0a, /*         ##  ##   */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x7f, /*   ############## */
		(byte) 0x49, /*   ##    ##    ## */
		/* Charackter #179  */
		(byte) 0x32, /*     ####    ##   */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x32, /*     ####    ##   */
		/* Charackter #180  */
		(byte) 0x32, /*     ####    ##   */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x32, /*     ####    ##   */
		/* Charackter #181  */
		(byte) 0x32, /*     ####    ##   */
		(byte) 0x4a, /*   ##    ##  ##   */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x30, /*     ####         */
		/* Charackter #182  */
		(byte) 0x3a, /*     ######  ##   */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x41, /*   ##          ## */
		(byte) 0x21, /*     ##        ## */
		(byte) 0x7a, /*   ########  ##   */
		/* Charackter #183  */
		(byte) 0x3a, /*     ######  ##   */
		(byte) 0x42, /*   ##        ##   */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		(byte) 0x78, /*   ########       */
		/* Charackter #184  */
		(byte) 0x00, /*                  */
		(byte) 0x9d, /* ##    ######  ## */
		(byte) 0xa0, /* ##  ##           */
		(byte) 0xa0, /* ##  ##           */
		(byte) 0x7d, /*   ##########  ## */
		/* Charackter #185  */
		(byte) 0x39, /*     ######    ## */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x39, /*     ######    ## */
		/* Charackter #186  */
		(byte) 0x3d, /*     ########  ## */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x3d, /*     ########  ## */
		/* Charackter #187  */
		(byte) 0x3c, /*     ########     */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0xff, /* ################ */
		(byte) 0x24, /*     ##    ##     */
		(byte) 0x24, /*     ##    ##     */
		/* Charackter #188  */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x7e, /*   ############   */
		(byte) 0x49, /*   ##    ##    ## */
		(byte) 0x43, /*   ##        #### */
		(byte) 0x66, /*   ####    ####   */
		/* Charackter #189  */
		(byte) 0x2b, /*     ##  ##  #### */
		(byte) 0x2f, /*     ##  ######## */
		(byte) 0xfc, /* ############     */
		(byte) 0x2f, /*     ##  ######## */
		(byte) 0x2b, /*     ##  ##  #### */
		/* Charackter #190  */
		(byte) 0xff, /* ################ */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0xf6, /* ########  ####   */
		(byte) 0x20, /*     ##           */
		/* Charackter #191  */
		(byte) 0xc0, /* ####             */
		(byte) 0x88, /* ##      ##       */
		(byte) 0x7e, /*   ############   */
		(byte) 0x09, /*         ##    ## */
		(byte) 0x03, /*             #### */
		/* Charackter #192  */
		(byte) 0x20, /*     ##           */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x54, /*   ##  ##  ##     */
		(byte) 0x79, /*   ########    ## */
		(byte) 0x41, /*   ##          ## */
		/* Charackter #193  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x44, /*   ##      ##     */
		(byte) 0x7d, /*   ##########  ## */
		(byte) 0x41, /*   ##          ## */
		/* Charackter #194  */
		(byte) 0x30, /*     ####         */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x4a, /*   ##    ##  ##   */
		(byte) 0x32, /*     ####    ##   */
		/* Charackter #195  */
		(byte) 0x38, /*     ######       */
		(byte) 0x40, /*   ##             */
		(byte) 0x40, /*   ##             */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x7a, /*   ########  ##   */
		/* Charackter #196  */
		(byte) 0x00, /*                  */
		(byte) 0x7a, /*   ########  ##   */
		(byte) 0x0a, /*         ##  ##   */
		(byte) 0x0a, /*         ##  ##   */
		(byte) 0x72, /*   ######    ##   */
		/* Charackter #197  */
		(byte) 0x7d, /*   ##########  ## */
		(byte) 0x0d, /*         ####  ## */
		(byte) 0x19, /*       ####    ## */
		(byte) 0x31, /*     ####      ## */
		(byte) 0x7d, /*   ##########  ## */
		/* Charackter #198  */
		(byte) 0x26, /*     ##    ####   */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x2f, /*     ##  ######## */
		(byte) 0x28, /*     ##  ##       */
		/* Charackter #199  */
		(byte) 0x26, /*     ##    ####   */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x29, /*     ##  ##    ## */
		(byte) 0x26, /*     ##    ####   */
		/* Charackter #200  */
		(byte) 0x30, /*     ####         */
		(byte) 0x48, /*   ##    ##       */
		(byte) 0x4d, /*   ##    ####  ## */
		(byte) 0x40, /*   ##             */
		(byte) 0x20, /*     ##           */
		/* Charackter #201  */
		(byte) 0x38, /*     ######       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		/* Charackter #202  */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x08, /*         ##       */
		(byte) 0x38, /*     ######       */
		/* Charackter #203  */
		(byte) 0x2f, /*     ##  ######## */
		(byte) 0x10, /*       ##         */
		(byte) 0xc8, /* ####    ##       */
		(byte) 0xac, /* ##  ##  ####     */
		(byte) 0xba, /* ##  ######  ##   */
		/* Charackter #204  */
		(byte) 0x2f, /*     ##  ######## */
		(byte) 0x10, /*       ##         */
		(byte) 0x28, /*     ##  ##       */
		(byte) 0x34, /*     ####  ##     */
		(byte) 0xfa, /* ##########  ##   */
		/* Charackter #205  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x7b, /*   ########  #### */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #206  */
		(byte) 0x08, /*         ##       */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x2a, /*     ##  ##  ##   */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x22, /*     ##      ##   */
		/* Charackter #207  */
		(byte) 0x22, /*     ##      ##   */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x2a, /*     ##  ##  ##   */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x08, /*         ##       */
		/* Charackter #208  */
		(byte) 0xaa, /* ##  ##  ##  ##   */
		(byte) 0x00, /*                  */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0x00, /*                  */
		(byte) 0xaa, /* ##  ##  ##  ##   */
		/* Charackter #209  */
		(byte) 0xaa, /* ##  ##  ##  ##   */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0xaa, /* ##  ##  ##  ##   */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0xaa, /* ##  ##  ##  ##   */
		/* Charackter #210  */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0xaa, /* ##  ##  ##  ##   */
		(byte) 0x55, /*   ##  ##  ##  ## */
		(byte) 0xaa, /* ##  ##  ##  ##   */
		(byte) 0x55, /*   ##  ##  ##  ## */
		/* Charackter #211  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		(byte) 0x00, /*                  */
		/* Charackter #212  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0xff, /* ################ */
		(byte) 0x00, /*                  */
		/* Charackter #213  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0xff, /* ################ */
		(byte) 0x00, /*                  */
		/* Charackter #214  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0xff, /* ################ */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		/* Charackter #215  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0xf0, /* ########         */
		(byte) 0x10, /*       ##         */
		(byte) 0xf0, /* ########         */
		/* Charackter #216  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0xfc, /* ############     */
		(byte) 0x00, /*                  */
		/* Charackter #217  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0xf7, /* ########  ###### */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		/* Charackter #218  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		/* Charackter #219  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0xf4, /* ########  ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0xfc, /* ############     */
		/* Charackter #220  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x17, /*       ##  ###### */
		(byte) 0x10, /*       ##         */
		(byte) 0x1f, /*       ########## */
		/* Charackter #221  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x1f, /*       ########## */
		(byte) 0x10, /*       ##         */
		(byte) 0x1f, /*       ########## */
		/* Charackter #222  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x1f, /*       ########## */
		(byte) 0x00, /*                  */
		/* Charackter #223  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0xf0, /* ########         */
		(byte) 0x00, /*                  */
		/* Charackter #224  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x1f, /*       ########## */
		(byte) 0x10, /*       ##         */
		/* Charackter #225  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x1f, /*       ########## */
		(byte) 0x10, /*       ##         */
		/* Charackter #226  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0xf0, /* ########         */
		(byte) 0x10, /*       ##         */
		/* Charackter #227  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		(byte) 0x10, /*       ##         */
		/* Charackter #228  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		/* Charackter #229  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0xff, /* ################ */
		(byte) 0x10, /*       ##         */
		/* Charackter #230  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #231  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		/* Charackter #232  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x1f, /*       ########## */
		(byte) 0x10, /*       ##         */
		(byte) 0x17, /*       ##  ###### */
		/* Charackter #233  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xfc, /* ############     */
		(byte) 0x04, /*           ##     */
		(byte) 0xf4, /* ########  ##     */
		/* Charackter #234  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x17, /*       ##  ###### */
		(byte) 0x10, /*       ##         */
		(byte) 0x17, /*       ##  ###### */
		/* Charackter #235  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0xf4, /* ########  ##     */
		(byte) 0x04, /*           ##     */
		(byte) 0xf4, /* ########  ##     */
		/* Charackter #236  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		(byte) 0x00, /*                  */
		(byte) 0xf7, /* ########  ###### */
		/* Charackter #237  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #238  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0xf7, /* ########  ###### */
		(byte) 0x00, /*                  */
		(byte) 0xf7, /* ########  ###### */
		/* Charackter #239  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x17, /*       ##  ###### */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #240  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x1f, /*       ########## */
		(byte) 0x10, /*       ##         */
		(byte) 0x1f, /*       ########## */
		/* Charackter #241  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0xf4, /* ########  ##     */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #242  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0xf0, /* ########         */
		(byte) 0x10, /*       ##         */
		(byte) 0xf0, /* ########         */
		/* Charackter #243  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x1f, /*       ########## */
		(byte) 0x10, /*       ##         */
		(byte) 0x1f, /*       ########## */
		/* Charackter #244  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x1f, /*       ########## */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #245  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xfc, /* ############     */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #246  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xf0, /* ########         */
		(byte) 0x10, /*       ##         */
		(byte) 0xf0, /* ########         */
		/* Charackter #247  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0xff, /* ################ */
		(byte) 0x10, /*       ##         */
		(byte) 0xff, /* ################ */
		/* Charackter #248  */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0x14, /*       ##  ##     */
		(byte) 0xff, /* ################ */
		(byte) 0x14, /*       ##  ##     */
		/* Charackter #249  */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x10, /*       ##         */
		(byte) 0x1f, /*       ########## */
		(byte) 0x00, /*                  */
		/* Charackter #250  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xf0, /* ########         */
		(byte) 0x10, /*       ##         */
		/* Charackter #251  */
		(byte) 0xff, /* ################ */
		(byte) 0xff, /* ################ */
		(byte) 0xff, /* ################ */
		(byte) 0xff, /* ################ */
		(byte) 0xff, /* ################ */
		/* Charackter #252  */
		(byte) 0xf0, /* ########         */
		(byte) 0xf0, /* ########         */
		(byte) 0xf0, /* ########         */
		(byte) 0xf0, /* ########         */
		(byte) 0xf0, /* ########         */
		/* Charackter #253  */
		(byte) 0xff, /* ################ */
		(byte) 0xff, /* ################ */
		(byte) 0xff, /* ################ */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		/* Charackter #254  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0x00, /*                  */
		(byte) 0xff, /* ################ */
		(byte) 0xff, /* ################ */
		/* Charackter #255  */
		(byte) 0x0f, /*         ######## */
		(byte) 0x0f, /*         ######## */
		(byte) 0x0f, /*         ######## */
		(byte) 0x0f, /*         ######## */
		(byte) 0x0f, /*         ######## */
	};
	public void setText(String s) {
		text=s;
	}
	public void setTextSize(int h) {
		chh=h;
		chw=5*(chh-1)/7+1;
	}

	public void setTextColor(int c) {
		tc=c;
	}

	public OLEDView(Context context) {
		super(context);
		initOLEDView();        
	}
	public OLEDView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initOLEDView();  
	}
	public OLEDView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		initOLEDView();  
	}
	protected void initOLEDView() {
		//setFocusable(true); //necessary for getting the touch events
		//   setFocusableInTouchMode(true);
		//   this.setOnTouchListener(this);
		//  Resources r = this.getResources();
		paint=new Paint();
		//  paint.setStrokeWidth(1);
		//  paint.setStyle(Paint.Style.STROKE);
		paint.setAntiAlias(false);
		paint.setStyle(Style.FILL_AND_STROKE);
		paint.setColor(Color.BLUE);
		//   paint.setTextSize(20);
		Log.d("xxx","init");
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int modex=MeasureSpec.getMode(widthMeasureSpec);
		int sizex=MeasureSpec.getSize(widthMeasureSpec);
		int modey=MeasureSpec.getMode(heightMeasureSpec);
		int sizey=MeasureSpec.getSize(heightMeasureSpec);

		if (modex == MeasureSpec.UNSPECIFIED) { 
			sw=text.length()*chw;
		}	else if (modex == MeasureSpec.AT_MOST) { 
			sw=sizex;
			if(sw>text.length()*chw) sw=text.length()*chw;
		} else sw=sizex;
		if (modey == MeasureSpec.UNSPECIFIED) { 
			Log.d("xxx","yunspec");
			sh=chh;
		}	else if (modey == MeasureSpec.AT_MOST) { 
			sh=sizey;
			if(sh>chh) sh=chh;
		} else sh=sizey;
		setMeasuredDimension(sw, sh);
	}
        /* implements a fancy looking font (with shining, glow....) */
	void putchar(int x, int y,char c,Canvas canvas,int s) {
		int i,j; 
		int a=5*(int)c;  /* Position of Font data*/
		int nb=0;
		for(i=0;i<3;i++) nb+= ((tc>>i)&1);
		if(nb==0) return;
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		for(j=0;j<5;j++) {
			byte a1=font[a+j];
			for(i=0;i<7;i++) {
				if((a1>>i&1)!=0) {
					paint.setColor(Color.rgb(
							(15000/256*(tc&1)
									+0*((tc>>1)&1)
									+155*((tc>>2)&1))/nb,
									(30000/256*(tc&1)
											+155*((tc>>1)&1)
											+0*((tc>>2)&1))/nb,
											(30000/256*(tc&1)
													+0*((tc>>1)&1)
													+0*((tc>>2)&1))/nb));

					canvas.drawRect(x+j*s,y+i*s,x+j*s+(s),y+i*s+(s),paint);

					paint.setColor(Color.rgb(
							(0*(tc&1)
									+10*((tc>>1)&1)
									+255*((tc>>2)&1))/nb,
									(60000/256*(tc&1)
											+255*((tc>>1)&1)
											+50*((tc>>2)&1))/nb,
											(255*(tc&1)
													+0*((tc>>1)&1)
													+0*((tc>>2)&1))/nb));

					canvas.drawRect(x+j*s,y+i*s,x+j*s+(s-1),y+i*s+(s-1),paint);
				} else {
					paint.setColor(Color.rgb(
							(10000/256*(tc&1)
									+10000/256*((tc>>1)&1)
									+15000/256*((tc>>2)&1))/nb,
									(15000/256*(tc&1)
											+15000/256*((tc>>1)&1)
											+10000/256*((tc>>2)&1))/nb,
											(15000/256*(tc&1)
													+10000/256*((tc>>1)&1)
													+10000/256*((tc>>2)&1))/nb));
					canvas.drawRect(x+j*s,y+i*s,x+j*s+(s-1),y+i*s+(s-1),paint);
				}
			}
		}	
	}

	@Override
	public void onDraw(Canvas canvas) {
		sw=getMeasuredWidth();
		sh=getMeasuredHeight();
		// canvas.drawColor(Color.BLACK);
		//  paint.setStyle(Paint.Style.STROKE);
		//  paint.setColor(Color.YELLOW);
		//  canvas.drawRect(sx,sy,sx+sw,sy+sh,paint);
		paint.setAntiAlias(true);
		//   paint.setStyle(Paint.Style.FILL_AND_STROKE);
		int ssx=(sw-text.length()*chw)/2;
		for(int i=0;i<text.length();i++) putchar(ssx+chw*i,0,text.toCharArray()[i], canvas,chw/5);
	}
}
