package de.hoffmannsgimmicks;

/* MeetingUhrInfoActivity.java (c) 2011-2015 by Markus Hoffmann 
 *
 * This file is part of MeetingCostClock for Android 
 * ============================================================
 * MeetingCostClock for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MeetingUhrInfoActivity extends Activity {
	private Button fertig;
	private TextView readme;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);
		fertig=(Button) findViewById(R.id.okbutton);
		readme=(TextView)findViewById(R.id.description);
		readme.setText(Html.fromHtml(getResources().getString(R.string.readme)+
        		getResources().getString(R.string.news)+getResources().getString(R.string.impressum)));
		fertig.setOnClickListener(new OnClickListener() {public void onClick(View v) {finish();}});
    }
}
