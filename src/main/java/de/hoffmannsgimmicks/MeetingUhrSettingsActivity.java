package de.hoffmannsgimmicks;

/* MeetingUhrSettingsActivity.java (c) 2011-2015 by Markus Hoffmann 
 *
 * This file is part of MeetingCostClock for Android 
 * ============================================================
 * MeetingCostClock for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Activity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MeetingUhrSettingsActivity extends Activity {
	private TextView ed1,ed2,ed3,t12;
	private static SeekBar duration,participants,rate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		ed1=(TextView) findViewById(R.id.editText1);
		ed2=(TextView) findViewById(R.id.editText2);
		ed3=(TextView) findViewById(R.id.editText3);
		t12=(TextView) findViewById(R.id.text12);

		ed1.setText(""+MeetingUhrActivity.price/100.0);
		ed3.setText(""+MeetingUhrActivity.dauer);
		ed2.setText(""+MeetingUhrActivity.people);
		duration=(SeekBar) findViewById(R.id.duration);
		participants=(SeekBar) findViewById(R.id.participants);
		rate=(SeekBar) findViewById(R.id.rate);

		duration.setMax(180);
		duration.setProgress(MeetingUhrActivity.dauer);
		participants.setMax(100);
		participants.setProgress(MeetingUhrActivity.people);


		int[] factors=getResources().getIntArray(R.array.currency_factors);
		String[] ccc=getResources().getStringArray(R.array.currency_values);

		rate.setMax(10000);  /* times currecny factor*/

		int i;
		for (i=0;i<factors.length;i++) {
			if(ccc[i].equals(MeetingUhrActivity.mcurrency)) rate.setMax(100*factors[i]); 
		}
		rate.setProgress(MeetingUhrActivity.price);
		t12.setText(MeetingUhrActivity.mcurrency);

		duration.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

				MeetingUhrActivity.dauer=progress;
				ed3.setText(""+MeetingUhrActivity.dauer);
				ed3.invalidate();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}
		});
		participants.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

				MeetingUhrActivity.people=progress;
				ed2.setText(""+MeetingUhrActivity.people);
				ed2.invalidate();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}
		});
		rate.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

				MeetingUhrActivity.price=progress;
				ed1.setText(""+MeetingUhrActivity.price/100.0);
				ed1.invalidate();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	public void onPause() {
		super.onPause();
		MeetingUhrActivity.redraw();
	}
}
