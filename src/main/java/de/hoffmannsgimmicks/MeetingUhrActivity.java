package de.hoffmannsgimmicks;

/* MeetingUhrActivity.java (c) 2011-2015 by Markus Hoffmann 
 *
 * This file is part of MeetingCostClock for Android 
 * ============================================================
 * MeetingCostClock for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MeetingUhrActivity extends Activity {
	public static int people=5;
	public static int price=4711;  /* measured in cent */
	public static int dauer=60;
	public static boolean running=false;
	public static double seconds=0;
	public static int minutes=0;
	public static double starttimer,timerrel;
	public static double timeintegrated;
	public static double priceintegrated;
	public static int count;
	private static TextView peoples,euro;
	private static OLEDView time;
	public static String mcurrency="€";
	
	private Button plus,minus;
	private Button startstopp,reset;
	private static ProgressBar pbar;
	
	private RefreshHandler _redrawHandler = new RefreshHandler();

	class RefreshHandler extends Handler {
		@Override
		public void handleMessage(Message message) {
			MeetingUhrActivity.this.update();
			// MeetingUhrActivity.this.invalidate();
		}
		public void sleep(long delayMillis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		plus=(Button) findViewById(R.id.plus);
		minus=(Button) findViewById(R.id.minus);
		startstopp=(Button) findViewById(R.id.startstop);
		reset=(Button) findViewById(R.id.reset);
    	
		peoples=(TextView) findViewById(R.id.people);
		euro=(TextView) findViewById(R.id.euro);
		time=(OLEDView) findViewById(R.id.time);
		pbar=(ProgressBar) findViewById(R.id.progress);
    	
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		mcurrency=prefs.getString("select_currency", "€");
		if(price==4711) price=(int)(Double.parseDouble(prefs.getString("select_price", "20.00"))*100.0);
    	
		peoples.setText((String)""+people);
		euro.setText((String)""+(int)priceintegrated+mcurrency);
		pbar.setMax(1000);
    	
		time.setText("00:00");
		time.setTextColor(1);
		time.setTextSize(24);
    	
		reset.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				seconds=0;
				minutes=0;
				priceintegrated=0;
				timerrel=starttimer=android.os.SystemClock.elapsedRealtime()/1000.0;
				count=0;
				redraw();
			}
		});	
		startstopp.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(running) {
					running=false;
					startstopp.setText("START");
				} else {
					running=true;
					startstopp.setText("STOP");
					starttimer=timerrel=android.os.SystemClock.elapsedRealtime()/1000.0;
				}
				setMode(running);
			}
		});	
		plus.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				people++;
				peoples.setText((String)""+people);
			}
		});
		minus.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				people--;
				if(people<0) people=0;
 				peoples.setText((String)""+people);
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
    /**
     * Called when a menu item is selected.
     */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent viewIntent;
		switch (item.getItemId()) {
		case R.id.infotext:    
 			viewIntent = new Intent(MeetingUhrActivity.this, MeetingUhrInfoActivity.class);
			startActivity(viewIntent);
			break;
            case R.id.text:     
            	viewIntent = new Intent(MeetingUhrActivity.this, MeetingUhrSettingsActivity.class);
            	startActivity(viewIntent);
            	break;
            case R.id.settings:     
            	viewIntent = new Intent(MeetingUhrActivity.this, PreferencesActivity.class);
            	startActivity(viewIntent);
            	break;
           case R.id.finish: 
            	finish();
                break;
            default: 
            	return super.onOptionsItemSelected(item);
        }
        return true;
    }
    
    public void setMode(boolean mode) {
        if (mode) {
            update();
            return;
        } else  {
            // TODO: implement.
        }
    }
    private void update() {
    	double elapsedtime,totetime;
    	redraw();
        	count++;
        	elapsedtime=android.os.SystemClock.elapsedRealtime()/1000.0-timerrel;
        	timerrel+=elapsedtime;
        	totetime=timerrel-starttimer;
        	seconds=totetime%60;
        	minutes=(int)totetime/60;
        	priceintegrated+=people*price*elapsedtime/360000.0;
            if(running) _redrawHandler.sleep(1000);
        }
    public static void redraw() {
    	peoples.setText((String)""+people);
    	peoples.postInvalidate();
    	euro.setText((String)""+(int)priceintegrated+mcurrency);
    	if(timerrel-starttimer<(dauer-5)*60) euro.setTextColor(Color.WHITE);
    	else if(timerrel-starttimer<dauer*60) euro.setTextColor(Color.YELLOW);
    	else  euro.setTextColor(Color.RED);

    	euro.postInvalidate();
    	if(minutes>=100) String.format("%3d:%02d",minutes,(int)seconds);
    	else time.setText((String) String.format("%02d:%02d",minutes,(int)seconds));
    	time.postInvalidate();
    	if((timerrel-starttimer)>dauer*60) pbar.setIndeterminate(true);
    	else {
    		pbar.setIndeterminate(false);
    		pbar.setSecondaryProgress((int)((timerrel-starttimer)/dauer/60*1000));
    	}
    }
}
