Guide to contributing to Meeting Cost Clock for Android
========================================================

The App has been in the Google Play Store from 2011 to 2014.

I have stopped development on this app myself, because I think it is quite
complete. If someone wants to further contribute I suggest following
improvements:

1. Translate the included help/documentation of all settings into more languages
(and complete the german translation).

2. improve the look-and-feel al little further.

3. Maybe add new features for speakers. But KISS!


best regards
Markus Hoffmann, October 2015

