Meeting Cost Clock for Android
==============================
(c) 2011-2015 by Markus Hoffmann <kollo@users.sourceforge.net>

It can show, how expensive a meeting will be.

The Meeting-Cost-Clock is a clock, which measures the time in a currency 
value (EURO or Dollar), 

It can show, how expensive a meeting will be taking into account the number of
participants and a mean value of a price per person per hour. 

Press MENU-->Settings to confgure.
You can adjust the number of participants with the + and - Buttons 
(even during the meeting). 

Place the Meeting-Cost-Clock well visible for everybody on the table. 

Time is Money.

A good meeting should be only attended by people which can contribute.
A good meeting will not take longer than an hour without a break.

The best meeting is the one, which need not take place. 


Sitzungs-Kosten-Uhr fuer Android
================================
(c) 2011-2015 by Markus Hoffmann <kollo@users.sourceforge.net>

Zeigt auf, wie teuer eine Sitzung wird.

Die Sitzungs-Kosten-Uhr ist eine Stoppuhr mit der Besonderheit,  daß sie die
Zeit in EURO mißt. Sie kann aufzeigen, wie teuer eine Sitzung wird.  Hierzu
werden die Anzahl der Teilnehmer und ein durchschnittlicher Stundensatz 
berücksichtigt. 

Der Stundensatz und noch andere Einstellungen können über  MENU->Settings
vorgenommen werden. Die Anzahl der Teilnehmer kann mit den +  und - Tasten
angepasst werden, auch noch während der Sitzung.

Stellen Sie die Uhr gut sichbar für alle Teilnehmer auf, und erläutern Sie
ihren Sinn; nämlich die Siztung möglichst effizient zu gestalten, denn 
###Zeit ist Geld###.

Eine gute Sitzung sollte nur Teilnehmer haben, welche auch beitragen. Eine gute 
Sitzung dauert nie länger als eine Stunde ohne Pause.

Die beste Sitzung ist die, welche nicht stattzufinden braucht.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
